﻿CREATE TABLE TAccount ( 
    Id         VARCHAR( 38 )   PRIMARY KEY,
    ActName    VARCHAR( 50 ),
    ActNamePY  VARCHAR( 25 ),
    Remark     VARCHAR( 100 ),
    Balance    NUMERIC,
    UseCount   INT,
    OperatorId VARCHAR( 38 ),
    AddTime    DATETIME,
    LastTime   DATETIME 
);
CREATE TABLE TConsumeType ( 
    Id         VARCHAR( 38 )   PRIMARY KEY,
    TypeName   VARCHAR( 50 ),
    TypeNamePY VARCHAR( 25 ),
    UseCount   INT,
    Category   INT,
    IsPublic   BOOLEAN,
    Remark     VARCHAR( 100 ),
    OperatorId VARCHAR( 38 ),
    LastTime   DATETIME,
    AddTime    DATETIME 
);
CREATE TABLE TConsume ( 
    Id            VARCHAR( 38 )   PRIMARY KEY
                                  NOT NULL
                                  UNIQUE,
    ConsumeName   VARCHAR( 50 ),
    ConsumeTypeId VARCHAR( 38 ),
    BalanceTypeId INT,
    AccountId     VARCHAR( 38 ),
    Amount        NUMERIC,
    Balance       NUMERIC,
    Remark        VARCHAR( 100 ),
    ConsumeDate   DATETIME,
    IsCancel      BOOLEAN         DEFAULT ( 0 ),
    AddTime       DATETIME,
    LastTime      DATETIME,
    OperatorId    VARCHAR( 38 ) 
);
CREATE TABLE TOperator ( 
    Id         VARCHAR( 38 )  PRIMARY KEY,
    Name       VARCHAR( 20 ),
    NamePY     VARCHAR( 20 ),
    LoginName  VARCHAR( 20 ),
    LoginPwd   VARCHAR( 50 ),
    AddTime    DATETIME,
    LastTime   DATETIME,
    OperatorId VARCHAR( 38 ),
    IsDel      BOOLEAN 
);
CREATE TABLE TDictionary ( 
    DictKey         INT,
    DictKeyPrompt   VARCHAR( 50 ),
    DictValue       INT,
    DictValuePrompt VARCHAR( 50 ) 
);

Create View VConsume
as
select 
    a.id,a.ConsumeName,a.ConsumeTypeId,c.TypeName as ConsumeTypeName,a.BalanceTypeId,
    d.DictValuePrompt as BalanceTypeName,a.AccountId,b.ActName as AccountName,a.Amount,
    a.Balance,a.Remark,a.ConsumeDate,a.IsCancel,a.AddTime,a.LastTime,a.OperatorId
From TConsume a
inner join TAccount b on a.AccountId=b.Id
left join TConsumeType c on a.ConsumeTypeId = c.Id
inner join TDictionary d on a.BalanceTypeId = d.DictValue and d.DictKey=1;

Create View VConsumeType
as
select 
	a.id,a.TypeName,a.TypeNamePY,a.UseCount,a.Category,a.IsPublic,
	b.DictValuePrompt as CategoryName,a.Remark,a.OperatorId,c.Name as OperatorName,a.LastTime,a.AddTime
from TConsumeType a
inner join TDictionary b on a.Category = b.DictValue and b.DictKey= 2
inner join TOperator c on a.OperatorId = c.Id


--数据初始化

delete from TDictionary;
insert into TDictionary(DictKey,DictKeyPrompt,DictValue,DictValuePrompt)
select 1,'资金状态',1001,'支出' union all
select 1,'资金状态',1002,'收入' union all
select 1,'资金状态',1003,'支出冲账' union all
select 1,'资金状态',1004,'收入冲账' union all
select 1,'资金状态',1005,'对账支出' union all
select 1,'资金状态',1006,'对账收入' union all
select 1,'资金状态',1007,'转账支出' union all
select 1,'资金状态',1008,'转账收入' union all
select 2,'账目类型分类',1,'公共' union all
select 2,'账目类型分类',2,'支出' union all
select 2,'账目类型分类',3,'收入';

delete from TconsumeType;
insert into TConsumeType(Id,TypeName,TypeNamePY,Category,OperatorId,IsPublic)
select '100','未知','wz',1,'00',1 union all
select '101','其它','qt',1,'00',1 union all
select '001','房租','fz',2,'00',1 union all
select '002','水电费','sdf',2,'00',1 union all
select '003','电话费','dhf',2,'00',1 union all
select '004','还款','hk',2,'00',1 union all
select '005','伙食','hs',2,'00',1 union all
select '006','人情往来','rqwl',2,'00',1 union all
select '007','旅游娱乐','lyyl',2,'00',1 union all
select '008','工作预付','gzyf',2,'00',1 union all
select '009','交通费','jtf',2,'00',1 union all
select '501','工资','gz',3,'00',1 union all
select '502','工作报销','zzbx',3,'00',1 union all
select '503','理财','lc',3,'00',1 union all
select '504','礼金','lj',3,'00',1 union all
select '405','借款','jk',3,'00',1;


delete from TAccount;
insert into TAccount(Id,ActName,ActNamePY,OperatorId)
select '101','现金','XJ','00' union all
select '102','支付宝','ZFB','00' union all
select '103','公交卡','GJK','00' union all
select '104','广发信用卡','GFXYK','00' union all
select '105','工行工资卡','GHGZK','00';

delete from TOperator;
insert into TOperator(Id,Name,NamePY,LoginName,LoginPwd,OperatorId,IsDel)
select '00','管理员','GLY','admin','123','00',0;

delete from TConsume;