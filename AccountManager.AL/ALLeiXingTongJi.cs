﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALLeiXingTongJi : ALBase
    {
        public class SearchModel
        {
            public DateTime StartDate { set; get; }
            public DateTime EndDate { set; get; }
            public bool IsAddTime { set; get; }
            public string ConsumeTypeId { set; get; }
            public string AccountId { set; get; }
        }
        public class SearchResultModel
        {
            public string ConsumeTypeId { set; get; }
            public string ConsumeTypeName { set; get; }
            public decimal SumAmount { set; get; }
            public int UseCount { set; get; }
        }

        public DataTable GetLeiXingTongJiData(SearchModel serModel, out decimal sumAmount)
        {
            CommonSearch.Clear();
            string timeColumnsName = serModel.IsAddTime ? TConsume.CNAddTime : TConsume.CNConsumeDate;
            CommonSearch.And(timeColumnsName).GreaterThenEqual(serModel.StartDate.ToString("yyyy-MM-dd"));
            CommonSearch.And(timeColumnsName).LessThan(serModel.EndDate.AddDays(1).ToString("yyyy-MM-dd"));

            if (!string.IsNullOrEmpty(serModel.ConsumeTypeId))
            {
                CommonSearch.And(TConsume.CNConsumeTypeId).Equal(serModel.ConsumeTypeId);
            }
            if (!string.IsNullOrEmpty(serModel.AccountId))
            {
                CommonSearch.And(TConsume.CNAccountId).Equal(serModel.AccountId);
            }
            List<TConsume> consumeList = DA.GetList<TConsume>(CommonSearch);

            CommonSearch.Clear();
            CommonSearch.OrderByDesc(TConsumeType.CNUseCount);
            List<VConsumeType> cTypeList = DA.GetList<VConsumeType>(CommonSearch);
            List<SearchResultModel> sResultList = new List<SearchResultModel>();
            if (cTypeList != null)
            {
                foreach (var item in cTypeList)
                {
                    if (!string.IsNullOrEmpty(serModel.ConsumeTypeId) && serModel.ConsumeTypeId != item.Id)
                    {
                        continue;
                    }
                    sResultList.Add(
                        new SearchResultModel
                        {
                            ConsumeTypeId = item.Id,
                            ConsumeTypeName = item.TypeName,
                            UseCount = item.UseCount,
                            SumAmount = 0
                        });
                }
            }
            sumAmount = 0;
            if (consumeList != null)
            {
                foreach (var consumeitem in consumeList)
                {
                    foreach (var rResultItem in sResultList)
                    {
                        if (consumeitem.ConsumeTypeId == rResultItem.ConsumeTypeId)
                        {
                            rResultItem.SumAmount += consumeitem.Amount;
                        }
                        sumAmount += consumeitem.Amount;
                    }
                }
            }
            DataTable rTable = new DataTable();
            rTable.Columns.Add("ConsumeTypeName");
            rTable.Columns.Add("SumAmount");
            rTable.Columns.Add("UseCount");
            foreach (var item in sResultList)
            {
                rTable.Rows.Add(item.ConsumeTypeName, item.SumAmount, item.UseCount);
            }
            return rTable;
        }
    }
}
