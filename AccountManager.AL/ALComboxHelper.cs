﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALComboxHelper : ALBase
    {
        public DataTable GetBalanceTypeData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("BalanceTypeName");
            CommonSearch.Clear();
            CommonSearch.Add(TDictionary.CNDictKey).Equal(1);
            List<TDictionary> rList = DA.GetList<TDictionary>(CommonSearch);
            dt.Rows.Add(0, "全部");
            foreach (var item in rList)
            {
                dt.Rows.Add(item.DictValue, item.DictValuePrompt);
            }
            return dt;
        }

        public DataTable GetConsumeTypeCategory()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("CategoryName");
            CommonSearch.Clear();
            CommonSearch.Add(TDictionary.CNDictKey).Equal(2);
            List<TDictionary> rList = DA.GetList<TDictionary>(CommonSearch);            
            foreach (var item in rList)
            {
                dt.Rows.Add(item.DictValue, item.DictValuePrompt);
            }
            return dt;
        }
    }
}
