﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.AL.Enums
{
    /// <summary>
    /// 消费类型分类
    /// </summary>
    public enum EnConsumeTypeCategory
    {
        /// <summary>
        /// 未选中
        /// </summary>
        None = 0,
        /// <summary>
        /// 公共
        /// </summary>
        GongGong = 1,
        /// <summary>
        /// 支出
        /// </summary>
        ZhiChu = 2,
        /// <summary>
        /// 收入
        /// </summary>
        ShouRu = 3

    }
}
