﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;
using AccountManager.Runtime;

namespace AccountManager.AL
{
    public class ALZhuangZhang : ALBase
    {
        public class ZhuanZhangModel
        {
            public string OutAccountId { set; get; }
            public string InAccountId { set; get; }
            public string ConsumeTypeId { set; get; }
            public decimal Amount { set; get; }
            public string OutRemark { set; get; }
        }
        public bool ZhuanZhang(ZhuanZhangModel model, out string errMsg)
        {
            if (string.IsNullOrEmpty(model.OutAccountId) || string.IsNullOrEmpty(model.InAccountId))
            {
                errMsg = "转入账户和转出账户不能为空!";
                return false;
            }
            if (model.OutAccountId == model.InAccountId)
            {
                errMsg = "转入账户与转出账户相同!";
                return false;
            }
            if (string.IsNullOrEmpty(model.ConsumeTypeId))
            {
                errMsg = "账目类型不能为空!";
                return false;
            }
            if (model.Amount == 0)
            {
                errMsg = "转出金额不正确!";
                return false;
            }
            errMsg = null;
            try
            {
                DA.TransStart();
                TAccount outActModel = DA.GetModel<TAccount>(model.OutAccountId);
                outActModel.LastTime = GetNowTime();
                outActModel.Balance = outActModel.Balance - model.Amount;
                DA.Update(outActModel, TAccount.CNLastTime, TAccount.CNBalance);

                TAccount inActModel = DA.GetModel<TAccount>(model.InAccountId);
                inActModel.LastTime = GetNowTime();
                inActModel.Balance = inActModel.Balance + model.Amount;
                DA.Update(inActModel, TAccount.CNLastTime, TAccount.CNBalance);

                TConsume consumeModel = new TConsume();
                consumeModel.Id = GetGuid();
                consumeModel.ConsumeName = string.IsNullOrEmpty(model.OutRemark) ? "转账" : model.OutRemark;
                consumeModel.ConsumeTypeId = model.ConsumeTypeId;
                consumeModel.ConsumeDate = GetNowTime();
                consumeModel.AddTime = GetNowTime();
                consumeModel.LastTime = GetNowTime();
                consumeModel.BalanceTypeId = (int)EnBalanceType.ZhuanZhangZhiChu;
                consumeModel.AccountId = model.OutAccountId;
                consumeModel.Amount = model.Amount * -1;
                consumeModel.Balance = outActModel.Balance;
                consumeModel.OperatorId = LoginInfo.OperatorId;
                DA.Add(consumeModel);

                consumeModel.Id = GetGuid();
                consumeModel.AccountId = model.InAccountId;
                consumeModel.Amount = model.Amount;
                consumeModel.Balance = inActModel.Balance;
                consumeModel.BalanceTypeId = (int)EnBalanceType.ZhuanZhangShouRu;
                DA.Add(consumeModel);
                DA.TransCommit();
                return true;
            }
            catch (Exception ex)
            {
                DA.TransRollback();
                errMsg = "操作失败,详情请查看日志!";
                return false;
            }

        }
    }
}
