﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;
using AccountManager.Runtime;
using ZhCun.Framework.Common.Models;

namespace AccountManager.AL
{
    public class ALZhangMuLeiXing : ALBase
    {
        public List<VConsumeType> GetConsumeTypeData(string searchValue, int pageNo, int onePageCount, out int recordCount)
        {
            CommonSearch.Clear();
            if (!string.IsNullOrEmpty(searchValue))
            {
                CommonSearch
                    .Add(VConsumeType.CNTypeName).LikeFull(searchValue)
                    .Or(VConsumeType.CNTypeNamePY).LikeRight(searchValue);
            }
            CommonSearch.OrderByDesc(VConsumeType.CNUseCount);
            CommonSearch.OnePage = onePageCount;
            CommonSearch.PageNo = pageNo;
            List<VConsumeType> rList = DA.GetList<VConsumeType>(CommonSearch);
            recordCount = CommonSearch.RecordCount;
            return rList;
        }
        public bool AddConsumeType(TConsumeType model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch
                //.Add(TConsumeType.CNOperatorId).Equal(LoginInfo.OperatorId)
                //.Or(TConsumeType.CNIsPublic).Equal(true)
                //.AddGroup()
                .And(TConsumeType.CNTypeName).Equal(model.TypeName);
            bool r = DA.IsExist<TConsumeType>(CommonSearch);
            if (r)
            {
                errMsg = "该名称已存在!";
                return false;
            }
            errMsg = string.Empty;
            model.Id = GetGuid();
            model.OperatorId = LoginInfo.OperatorId;
            model.AddTime = GetNowTime();
            model.LastTime = GetNowTime();
            DA.Add(model);
            return true;
        }
        public bool UpdateConsumeType(TConsumeType model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch
                //.Add(TConsumeType.CNOperatorId).Equal(LoginInfo.OperatorId)
                //.Or(TConsumeType.CNIsPublic).Equal(true)
                //.AddGroup()
                .And(TConsumeType.CNTypeName).Equal(model.TypeName)
                .And(TConsumeType.CNId).EqualNot(model.Id);

            bool r = DA.IsExist<TConsumeType>(CommonSearch);
            if (r)
            {
                errMsg = "该名称已被其它用户占用!";
                return false;
            }
            errMsg = null;
            model.LastTime = GetNowTime();
            DA.Update(model,
                TConsumeType.CNLastTime,
                TConsumeType.CNTypeName,
                TConsumeType.CNTypeNamePY,
                TConsumeType.CNCategory,
                TConsumeType.CNRemark,
                TConsumeType.CNIsPublic
                );
            return true;
        }
        public bool DeleteConsumeType(VConsumeType selModel, out string errMsg)
        {
            bool typeIsUsed = DA.IsExist<TConsume>(TConsume.CNConsumeTypeId, selModel.Id);
            if (typeIsUsed)
            {
                errMsg = "当前类型已经使用不能删除!";
                return false;
            }
            errMsg = string.Empty;
            DA.Delete<TConsumeType>(selModel.Id);
            return true;
        }
    }
}