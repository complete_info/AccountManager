﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;
using AccountManager.Runtime;
using ZhCun.Framework.Common.Models;

namespace AccountManager.AL
{
    public class ALCaoZuoYuan : ALBase
    {        
        public bool AddOperator(TOperator model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch.Add(TOperator.CNLoginName).Equal(model.LoginName)
                .Or(TOperator.CNName).Equal(model.Name);
            bool isExist = DA.IsExist<TOperator>(CommonSearch);
            if (isExist)
            {
                errMsg = "用户名或登录名重复!";
                return false;
            }
            errMsg = null;
            model.LastTime = GetNowTime();
            model.AddTime = GetNowTime();
            model.Id = GetGuid();
            model.LoginPwd = "123";
            model.OperatorId = LoginInfo.OperatorId;
            DA.Add(model);
            return true;
        }

        public bool UpdateOperator(TOperator model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch.Add(TOperator.CNLoginName).Equal(model.LoginName)
                .Or(TOperator.CNName).Equal(model.Name)
                .AddGroup().And(TOperator.CNId).EqualNot(model.Id);
            bool isExist = DA.IsExist<TOperator>(CommonSearch);
            if (isExist)
            {
                errMsg = "用户名或登录名已被占用!";
                return false;
            }
            errMsg = null;
            model.LastTime = GetNowTime();
            DA.Update(model, TOperator.CNLastTime, TOperator.CNLoginName, TOperator.CNName, TOperator.CNNamePY);
            return true;
        }
        /// <summary>
        /// 根据搜索对象获取操作原列表
        /// </summary>
        public List<TOperator> GetOperatorList(ISearch serObj)
        {
            List<TOperator> rList = DA.GetList<TOperator>(serObj);            
            return rList;
        }
        /// <summary>
        /// 删除指定操作员
        /// </summary>
        public bool DeleteOperator(TOperator selModel, out string errMsg)
        {
            errMsg = string.Empty;
            DA.Delete<TOperator>(selModel.Id);
            return true;
        }
    }
}