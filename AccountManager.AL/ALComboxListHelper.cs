﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;

namespace AccountManager.AL
{
    public class ALComboxListHelper : ALBase
    {
        public DataTable GetAccountData(string searchValue)
        {
            CommonSearch.Clear();
            CommonSearch
                .Add(TAccount.CNActName).LikeFull(searchValue)
                .Or(TAccount.CNActNamePY).LikeFull(searchValue);
            CommonSearch.OrderByDesc(TAccount.CNUseCount).OrderByDesc(TAccount.CNLastTime);
            DataTable dt = DA.GetDataTable<TAccount>(CommonSearch);
            return dt;
        }
        /// <summary>
        /// 指定类型获取账目类型,始终包含公用分类
        /// </summary>
        public DataTable GetConsumeType(string searchValue, EnConsumeTypeCategory typeCategory)
        {
            CommonSearch.Clear();
            CommonSearch
                .Add(VConsumeType.CNTypeName).LikeFull(searchValue)
                .Or(VConsumeType.CNTypeNamePY).LikeRight(searchValue);
            if (typeCategory != EnConsumeTypeCategory.None)
            {
                CommonSearch.AddGroup();
                CommonSearch.And(VConsumeType.CNCategory).In((int)typeCategory, (int)EnConsumeTypeCategory.GongGong);                
            }
            CommonSearch.OrderByDesc(VConsumeType.CNUseCount).OrderByDesc(VConsumeType.CNLastTime);
            DataTable dt = DA.GetDataTable<VConsumeType>(CommonSearch);
            return dt;
        }
    }
}