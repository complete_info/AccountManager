﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALZiJinRiTongJi : ALBase
    {
        public class SearchModel
        {
            public DateTime StartDate { set; get; }
            public DateTime EndDate { set; get; }
            public bool IsAddTime { set; get; }
        }

        public DataTable GetData(SearchModel serModel)
        {
            Dictionary<string, Dictionary<int, decimal>> consumeGroup = new Dictionary<string, Dictionary<int, decimal>>();
            DataTable rTable = new DataTable();
            rTable.Columns.Add("日期");
            for (DateTime i = serModel.StartDate; i <= serModel.EndDate; i = i.AddDays(1))
            {
                string dateStr = i.ToString("yyyy-MM-dd");
                rTable.Rows.Add(dateStr);
                consumeGroup.Add(dateStr, null);
            }

            CommonSearch.Clear();
            string timeColumnsName = serModel.IsAddTime ? TConsume.CNAddTime : TConsume.CNConsumeDate;
            CommonSearch.Add(timeColumnsName).GreaterThenEqual(serModel.StartDate.ToString("yyyy-MM-dd"));
            CommonSearch.And(timeColumnsName).LessThan(serModel.EndDate.AddDays(1).ToString("yyyy-MM-dd"));
            CommonSearch.OrderByAsc(timeColumnsName);
            List<TConsume> consumeList = DA.GetList<TConsume>(CommonSearch);
            if (consumeList != null)
            {
                DateTime tmpTime;
                foreach (var item in consumeList)
                {
                    tmpTime = timeColumnsName == TConsume.CNAddTime ? item.AddTime : item.ConsumeDate;
                    string dateStr = tmpTime.ToString("yyyy-MM-dd");

                    if (consumeGroup[dateStr] == null)
                    {
                        consumeGroup[dateStr] = new Dictionary<int, decimal>();
                    }
                    if (!consumeGroup[dateStr].ContainsKey(item.BalanceTypeId))
                    {
                        consumeGroup[dateStr].Add(item.BalanceTypeId, 0);
                    }
                    consumeGroup[dateStr][item.BalanceTypeId] += item.Amount;
                }
            }

            //查询资金类型
            CommonSearch.Clear();
            CommonSearch.Add(TDictionary.CNDictKey).Equal(1);
            CommonSearch.OrderByAsc(TDictionary.CNDictValue);
            List<TDictionary> typeList = DA.GetList<TDictionary>(CommonSearch);

            foreach (var item in typeList)
            {
                DataColumn dc = new DataColumn(string.Format("{0}", item.DictValuePrompt));
                dc.DefaultValue = "0";
                dc.DataType = typeof(decimal);
                dc.Caption = item.DictValue.ToString();
                rTable.Columns.Add(dc);
            }

            for (int i = 0; i < rTable.Rows.Count; i++)
            {
                string dateStr = rTable.Rows[i][0].ToString();
                for (int j = 1; j < rTable.Columns.Count; j++)
                {
                    DataColumn dc = rTable.Columns[j];
                    if (consumeGroup[dateStr] != null && consumeGroup[dateStr].ContainsKey(int.Parse(dc.Caption)))
                    {
                        rTable.Rows[i][j] = consumeGroup[dateStr][int.Parse(dc.Caption)];
                    }
                }
            }
            return rTable;
        }
    }
}