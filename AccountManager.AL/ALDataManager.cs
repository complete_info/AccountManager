﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.Runtime;

namespace AccountManager.AL
{
    public class ALDataManager : ALBase
    {
        public void ClearConsume()
        {
            CommonSearch.Clear();
            CommonSearch.WhereText.AppendFormat("delete from TConsume where OperatorId='{0}';", LoginInfo.OperatorId);
            CommonSearch.WhereText.AppendFormat("update TAccount set Balance=0,LastTime='{1}' where OperatorId='{0}';", LoginInfo.OperatorId,DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            DA.ExecuteBySQL(CommonSearch);
        }

        public void ClearAllData()
        {
            CommonSearch.Clear();
            CommonSearch.WhereText.AppendFormat("delete from TConsume where OperatorId='{0}';", LoginInfo.OperatorId);
            CommonSearch.WhereText.AppendFormat("delete from TConsumeType where OperatorId='{0}';", LoginInfo.OperatorId);
            CommonSearch.WhereText.AppendFormat("delete from TAccount where OperatorId='{0}';", LoginInfo.OperatorId);
            DA.ExecuteBySQL(CommonSearch);            
        }

        public void ImportDefaultData()
        {
            //CommonSearch.Clear();
            //CommonSearch.WhereText.AppendFormat("insert into TConsumeType");
            //CommonSearch.WhereText.AppendFormat("Select * from TconsumeType where OperatorId='{0}'", LoginInfo.OperatorId);

            DA.ExecuteBySQL(CommonSearch);
        }
    }
}