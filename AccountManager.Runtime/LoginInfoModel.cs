﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.Runtime
{
    public class LoginInfoModel
    {
        public DateTime LoginTime { set; get; }
        public string LoginName { set; get; }
        public string Name { set; get; }
        public string OperatorId { set; get; }
    }
}
