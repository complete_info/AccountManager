﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using AccountManager.AL;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangHuDuiZhang : BaseForm
    {
        public FrmZhangHuDuiZhang()
            : this(null)
        {

        }
        public FrmZhangHuDuiZhang(TAccount actModel)
        {
            InitializeComponent();
            this._AccountMode = actModel;
        }

        TAccount _AccountMode;
        ALZhangHuDuiZhang _ALObj;

        private void FrmZhangHuDuiZhang_Load(object sender, EventArgs e)
        {
            ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, AL.Enums.EnConsumeTypeCategory.GongGong);
            if (_AccountMode != null)
            {
                dcm1.SetControlValue(_AccountMode);
                txtAccount.Text = _AccountMode.ActName;
                txtAccount.Tag = _AccountMode.Id;
                txtNewBalance.Focus();
            }
            else
            {
                ComboxListHelper.SetAccount(comboxList1, txtAccount);
                txtAccount.Focus();
            }
            _ALObj = new ALZhangHuDuiZhang();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!ShowQuestion("!确实要对账吗?对账后将产生账目流水,且不能恢复", "对账确认"))
            {
                return;
            }
            if (_AccountMode == null)
            {
                _AccountMode = new TAccount();
            }
            decimal newBalance;
            if (!decimal.TryParse(txtNewBalance.Text, out newBalance))
            {
                ShowMessage("新余额格式不合法!");
                txtNewBalance.Focus();
                txtNewBalance.SelectAll();
                return;
            }
            
            dcm1.SetValueToClassObj(_AccountMode);
            if (txtConsumeType.Tag == null)
            {
                ShowMessage("请选择对账的类型!");
                txtConsumeType.Focus();
                return;
            }
            if (txtAccount.Tag == null)
            {
                ShowMessage("对账账户不能为空!");
                txtAccount.Focus();
                return;
            }
            _AccountMode.Balance = newBalance;
            _AccountMode.Remark = txtConsumeType.Tag.ToString();

            string accountId = txtAccount.Tag.ToString();
            string consumeTypeId = txtConsumeType.Tag.ToString();


            string errMsg;
            bool r = _ALObj.VerifyAccount(accountId, consumeTypeId, newBalance, out errMsg);
            if (!r)
            {
                ShowMessage(errMsg);
            }
            else
            {
                MessageBox.Show("对账完成!");
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void txtNewBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtConsumeType.Focus();
            }
        }

        private void txtConsumeType_TextChanged(object sender, EventArgs e)
        {
            txtConsumeType.Tag = null;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboxList1_SelectedRowHandle(object sender, ZhCun.Framework.WinCommon.Components.SelectRowEventArgs e)
        {
            if (e.BindControl == txtAccount)
            {
                dcm1.SetControlValueFromDataRow(e.SelectedRow);
                if (txtBalance.Text == string.Empty)
                {
                    txtBalance.Text = "0";
                }
            }
        }
    }
}