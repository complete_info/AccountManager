﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangHuDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chBoxIsAddTime = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbSumAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.dcm = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.col_ConsumeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_BalanceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AddTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_IsCancel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.chBoxIsAddTime);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(821, 68);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(621, 27);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 25);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chBoxIsAddTime
            // 
            this.chBoxIsAddTime.AutoSize = true;
            this.chBoxIsAddTime.Checked = true;
            this.chBoxIsAddTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dcm.SetIsUse(this.chBoxIsAddTime, true);
            this.chBoxIsAddTime.Location = new System.Drawing.Point(445, 34);
            this.chBoxIsAddTime.Name = "chBoxIsAddTime";
            this.chBoxIsAddTime.Size = new System.Drawing.Size(108, 16);
            this.chBoxIsAddTime.TabIndex = 19;
            this.dcm.SetTagColumnName(this.chBoxIsAddTime, null);
            this.chBoxIsAddTime.Text = "按记账时间查询";
            this.dcm.SetTextColumnName(this.chBoxIsAddTime, "IsAddTime");
            this.chBoxIsAddTime.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 15;
            this.label1.Text = "开始日期:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "结束日期:";
            // 
            // dtpStartDate
            // 
            this.dcm.SetIsUse(this.dtpStartDate, true);
            this.dtpStartDate.Location = new System.Drawing.Point(79, 31);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(133, 21);
            this.dtpStartDate.TabIndex = 17;
            this.dcm.SetTagColumnName(this.dtpStartDate, null);
            this.dcm.SetTextColumnName(this.dtpStartDate, "StartTime");
            // 
            // dtpEndDate
            // 
            this.dcm.SetIsUse(this.dtpEndDate, true);
            this.dtpEndDate.Location = new System.Drawing.Point(288, 31);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(133, 21);
            this.dtpEndDate.TabIndex = 18;
            this.dcm.SetTagColumnName(this.dtpEndDate, null);
            this.dcm.SetTextColumnName(this.dtpEndDate, "EndTime");
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel3,
            this.tslbSumAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 566);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(821, 22);
            this.statusStrip1.TabIndex = 30;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel3.Text = "账户增加额：";
            // 
            // tslbSumAmount
            // 
            this.tslbSumAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbSumAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbSumAmount.Name = "tslbSumAmount";
            this.tslbSumAmount.Size = new System.Drawing.Size(20, 17);
            this.tslbSumAmount.Text = "０";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ConsumeDate,
            this.col_ConsumeName,
            this.col_AccountName,
            this.col_Amount,
            this.col_Balance,
            this.col_BalanceType,
            this.col_ConsumeType,
            this.col_AddTime,
            this.col_IsCancel});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 68);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(821, 498);
            this.dgv.TabIndex = 31;
            this.dgv.UseControlStyle = true;
            // 
            // col_ConsumeDate
            // 
            this.col_ConsumeDate.DataPropertyName = "ConsumeDate";
            this.col_ConsumeDate.HeaderText = "账目日期";
            this.col_ConsumeDate.Name = "col_ConsumeDate";
            this.col_ConsumeDate.ReadOnly = true;
            // 
            // col_ConsumeName
            // 
            this.col_ConsumeName.DataPropertyName = "ConsumeName";
            this.col_ConsumeName.HeaderText = "账目说明";
            this.col_ConsumeName.Name = "col_ConsumeName";
            this.col_ConsumeName.ReadOnly = true;
            // 
            // col_AccountName
            // 
            this.col_AccountName.DataPropertyName = "AccountName";
            this.col_AccountName.HeaderText = "账户";
            this.col_AccountName.Name = "col_AccountName";
            this.col_AccountName.ReadOnly = true;
            this.col_AccountName.Visible = false;
            // 
            // col_Amount
            // 
            this.col_Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "C2";
            dataGridViewCellStyle3.NullValue = "0";
            this.col_Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_Amount.HeaderText = "发生额";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
            // 
            // col_Balance
            // 
            this.col_Balance.DataPropertyName = "Balance";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = "0";
            this.col_Balance.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_Balance.HeaderText = "账户余额";
            this.col_Balance.Name = "col_Balance";
            this.col_Balance.ReadOnly = true;
            // 
            // col_BalanceType
            // 
            this.col_BalanceType.DataPropertyName = "BalanceTypeName";
            this.col_BalanceType.HeaderText = "资金类型";
            this.col_BalanceType.Name = "col_BalanceType";
            this.col_BalanceType.ReadOnly = true;
            // 
            // col_ConsumeType
            // 
            this.col_ConsumeType.DataPropertyName = "ConsumeTypeName";
            this.col_ConsumeType.HeaderText = "账目类型";
            this.col_ConsumeType.Name = "col_ConsumeType";
            this.col_ConsumeType.ReadOnly = true;
            // 
            // col_AddTime
            // 
            this.col_AddTime.DataPropertyName = "AddTime";
            this.col_AddTime.HeaderText = "记账日期";
            this.col_AddTime.Name = "col_AddTime";
            this.col_AddTime.ReadOnly = true;
            // 
            // col_IsCancel
            // 
            this.col_IsCancel.DataPropertyName = "IsCancel";
            this.col_IsCancel.HeaderText = "冲账标志";
            this.col_IsCancel.Name = "col_IsCancel";
            this.col_IsCancel.ReadOnly = true;
            // 
            // FrmZhangHuDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 588);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangHuDetail";
            this.Text = "账目收支明细查看";
            this.Load += new System.EventHandler(this.FrmZhangHuDetail_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tslbSumAmount;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.CheckBox chBoxIsAddTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Button btnSearch;
        private ZhCun.Framework.WinCommon.Components.DCM dcm;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_BalanceType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AddTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_IsCancel;
    }
}