﻿namespace AccountManager.Notifys
{
    partial class FrmUpdateNotify
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNoShow = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.lklUpdate = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lbNoShow
            // 
            this.lbNoShow.AutoSize = true;
            this.lbNoShow.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbNoShow.Location = new System.Drawing.Point(193, 119);
            this.lbNoShow.Name = "lbNoShow";
            this.lbNoShow.Size = new System.Drawing.Size(74, 21);
            this.lbNoShow.TabIndex = 1;
            this.lbNoShow.TabStop = true;
            this.lbNoShow.Text = "不再提醒";
            this.lbNoShow.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbNoShow_LinkClicked);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(50, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 59);
            this.label1.TabIndex = 2;
            this.label1.Text = "    检测到当前程序有升级版本,是否进行升级?";
            // 
            // lklUpdate
            // 
            this.lklUpdate.AutoSize = true;
            this.lklUpdate.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lklUpdate.Location = new System.Drawing.Point(38, 119);
            this.lklUpdate.Name = "lklUpdate";
            this.lklUpdate.Size = new System.Drawing.Size(74, 21);
            this.lklUpdate.TabIndex = 1;
            this.lklUpdate.TabStop = true;
            this.lklUpdate.Text = "立即升级";
            this.lklUpdate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklUpdate_LinkClicked);
            // 
            // FrmUpdateNotify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 172);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lklUpdate);
            this.Controls.Add(this.lbNoShow);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmUpdateNotify";
            this.Controls.SetChildIndex(this.lbNoShow, 0);
            this.Controls.SetChildIndex(this.lklUpdate, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel lbNoShow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lklUpdate;
    }
}