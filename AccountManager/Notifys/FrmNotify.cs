﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AccountManager.Notifys
{
    public partial class FrmNotify : Form
    {
        public FrmNotify()
        {
            InitializeComponent();

            _notifyTimer = new Timer();
            _notifyTimer.Interval = 100;
            _notifyTimer.Tick += new EventHandler(NotifyTimer_Tick);
            _AutoCloseTimer = new Timer();
            _AutoCloseTimer.Interval = 3000;
            _AutoCloseTimer.Tick += new EventHandler(AutoCloseTimer_Tick);
            foreach (Control item in this.Controls)
            {
                item.MouseEnter += FrmNotify_MouseEnter;
            }
        }

        void AutoCloseTimer_Tick(object sender, EventArgs e)
        {
            HidWindow();
            _AutoCloseTimer.Stop();
        }

        Rectangle Rect;//定义一个存储矩形框的区域
        Timer _notifyTimer;
        Timer _AutoCloseTimer;
        FormState _NotifyState;
        bool _MouseIsEnter = false; //鼠标是否在窗体上
        bool _IsFirstSHow = true;
        /// <summary>
        /// 当前窗体状态
        /// </summary>
        enum FormState
        {
            //隐藏窗体
            Hide = 0,
            //显示窗体
            Display = 1,
            //隐藏窗体中
            Hiding = 3,
            //显示窗体中
            Displaying = 4,
        }
        
        private void FrmNotify_Click(object sender, EventArgs e)
        {
            HidWindow();
        }
        private void FrmNotify_MouseEnter(object sender, EventArgs e)
        {
            //鼠标进入
            _AutoCloseTimer.Stop();
            if (_NotifyState == FormState.Hiding)
            {
                _NotifyState = FormState.Displaying;
                _notifyTimer.Start();
            }
            _MouseIsEnter = true;
        }
        private void FrmNotify_MouseLeave(object sender, EventArgs e)
        {
            _AutoCloseTimer.Start();
        }
        private void NotifyTimer_Tick(object sender, EventArgs e)
        {
            switch (_NotifyState)
            {

                case FormState.Display:
                    this.SetBounds(Rect.X, Rect.Y + Rect.Height, Rect.Width, 0);//显示提示窗体，并把它放在屏幕底端
                    this.TopMost = true;
                    this.Show();
                    _NotifyState = FormState.Displaying;
                    break;
                case FormState.Displaying:
                    if (this.Height <= this.Rect.Height - 12)
                    {
                        //设定窗体的边界
                        this.SetBounds(Rect.X, this.Top - 12, Rect.Width, this.Height + 12);
                    }
                    else
                    {
                        //当窗体完全显示时
                        this.SetBounds(Rect.X, Rect.Y, Rect.Width, Rect.Height);//设定当前窗体的边界
                        _notifyTimer.Stop();
                        if (!_MouseIsEnter) _AutoCloseTimer.Start();
                    }
                    break;
                case FormState.Hide:
                    _NotifyState = FormState.Hiding;
                    break;
                case FormState.Hiding:
                    //当窗体没有完全隐藏时
                    if (this.Top <= this.Rect.Bottom - 12)
                    {
                        //设定控件的边界
                        this.SetBounds(Rect.X, this.Top + 12, Rect.Width, this.Height - 12);
                    }
                    else                                    //当窗体完全隐藏时
                    {
                        _notifyTimer.Stop();
                        this.Hide();                         //隐藏当前窗体                
                    }
                    break;

                default:
                    break;
            }
        }

        public virtual void ShowWindows()
        {
            if (_IsFirstSHow)
            {
                //初始化工作区的大小
                System.Drawing.Rectangle rect = System.Windows.Forms.Screen.GetWorkingArea(this);
                //为实例化的对象创建工作区域
                this.Rect = new System.Drawing.Rectangle(rect.Right - this.Width - 1, rect.Bottom - this.Height - 1, this.Width, this.Height);
                _IsFirstSHow = false;
            }
            _NotifyState = FormState.Display;
            lbTitle.Text = this.Text;
            _notifyTimer.Start();
        }
        public virtual void HidWindow()
        {            
            _NotifyState = FormState.Hide;
            _notifyTimer.Start();
        }
    }
}