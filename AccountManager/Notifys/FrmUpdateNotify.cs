﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AppUpdate;

namespace AccountManager.Notifys
{
    public partial class FrmUpdateNotify : FrmNotify
    {
        public FrmUpdateNotify()
        {
            InitializeComponent();
        }

        static bool IsNoShow;

        public override void ShowWindows()
        {
            if (!IsNoShow) base.ShowWindows();
        }
        private void lbNoShow_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IsNoShow = true;
            this.Hide();
            //this.HidWindow();
        }
        private void lklUpdate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IsNoShow = true;
            FrmAppUpdate frm = FrmAppUpdate.Create();
            frm.IsAutoStart = true;
            frm.ShowDialog();
            IsNoShow = false;
        }
    }
}