﻿using ZhCun.Framework.WinCommon.Controls;
namespace AccountManager
{
    partial class FrmJiZhang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtConsumeName = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAccount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAmount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRemark = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dtpConsumeDate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.comboxList1 = new ZhCun.Framework.WinCommon.Components.ComboxList(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "账目说明:";
            // 
            // txtConsumeName
            // 
            this.comboxList1.SetColumns(this.txtConsumeName, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeName, 0);
            this.txtConsumeName.EmptyTextTip = null;
            this.txtConsumeName.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeName, 0);
            this.dcm1.SetIsUse(this.txtConsumeName, true);
            this.comboxList1.SetIsUse(this.txtConsumeName, false);
            this.txtConsumeName.Location = new System.Drawing.Point(123, 55);
            this.txtConsumeName.Name = "txtConsumeName";
            this.comboxList1.SetNextControl(this.txtConsumeName, this.txtConsumeName);
            this.txtConsumeName.Size = new System.Drawing.Size(277, 21);
            this.txtConsumeName.TabIndex = 1;
            this.dcm1.SetTagColumnName(this.txtConsumeName, "Id");
            this.dcm1.SetTextColumnName(this.txtConsumeName, "ConsumeName");
            this.txtConsumeName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConsumeName_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(82, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "账户:";
            // 
            // txtAccount
            // 
            this.comboxList1.SetColumns(this.txtAccount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccount, 0);
            this.txtAccount.EmptyTextTip = "回车选择账户";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccount, 0);
            this.dcm1.SetIsUse(this.txtAccount, true);
            this.comboxList1.SetIsUse(this.txtAccount, false);
            this.txtAccount.Location = new System.Drawing.Point(123, 86);
            this.txtAccount.Name = "txtAccount";
            this.comboxList1.SetNextControl(this.txtAccount, this.txtConsumeType);
            this.txtAccount.Size = new System.Drawing.Size(277, 21);
            this.txtAccount.TabIndex = 2;
            this.dcm1.SetTagColumnName(this.txtAccount, "AccountId");
            this.dcm1.SetTextColumnName(this.txtAccount, "AccountName");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(58, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "账目类型:";
            // 
            // txtConsumeType
            // 
            this.comboxList1.SetColumns(this.txtConsumeType, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeType, 0);
            this.txtConsumeType.EmptyTextTip = "输入搜索内容后回车选择";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeType, 0);
            this.dcm1.SetIsUse(this.txtConsumeType, true);
            this.comboxList1.SetIsUse(this.txtConsumeType, false);
            this.txtConsumeType.Location = new System.Drawing.Point(123, 117);
            this.txtConsumeType.Name = "txtConsumeType";
            this.comboxList1.SetNextControl(this.txtConsumeType, this.txtAmount);
            this.txtConsumeType.Size = new System.Drawing.Size(277, 21);
            this.txtConsumeType.TabIndex = 3;
            this.dcm1.SetTagColumnName(this.txtConsumeType, "ConsumeTypeId");
            this.dcm1.SetTextColumnName(this.txtConsumeType, "ConsumeTypeName");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "发生额:";
            // 
            // txtAmount
            // 
            this.comboxList1.SetColumns(this.txtAmount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAmount, 0);
            this.txtAmount.EmptyTextTip = "输入发生额";
            this.txtAmount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAmount, 0);
            this.dcm1.SetIsUse(this.txtAmount, true);
            this.comboxList1.SetIsUse(this.txtAmount, false);
            this.txtAmount.Location = new System.Drawing.Point(123, 148);
            this.txtAmount.Name = "txtAmount";
            this.comboxList1.SetNextControl(this.txtAmount, this.txtAmount);
            this.txtAmount.Size = new System.Drawing.Size(277, 21);
            this.txtAmount.TabIndex = 5;
            this.dcm1.SetTagColumnName(this.txtAmount, null);
            this.dcm1.SetTextColumnName(this.txtAmount, "Amount");
            this.txtAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAmount_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(82, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "备注:";
            // 
            // txtRemark
            // 
            this.comboxList1.SetColumns(this.txtRemark, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtRemark, 0);
            this.txtRemark.EmptyTextTip = null;
            this.txtRemark.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtRemark, 0);
            this.dcm1.SetIsUse(this.txtRemark, true);
            this.comboxList1.SetIsUse(this.txtRemark, false);
            this.txtRemark.Location = new System.Drawing.Point(123, 179);
            this.txtRemark.Multiline = true;
            this.txtRemark.Name = "txtRemark";
            this.comboxList1.SetNextControl(this.txtRemark, this.txtRemark);
            this.txtRemark.Size = new System.Drawing.Size(277, 60);
            this.txtRemark.TabIndex = 6;
            this.dcm1.SetTagColumnName(this.txtRemark, null);
            this.dcm1.SetTextColumnName(this.txtRemark, "Remark");
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(72, 256);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(74, 29);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(342, 256);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 29);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dtpConsumeDate
            // 
            this.dcm1.SetIsUse(this.dtpConsumeDate, true);
            this.dtpConsumeDate.Location = new System.Drawing.Point(123, 23);
            this.dtpConsumeDate.Name = "dtpConsumeDate";
            this.dtpConsumeDate.Size = new System.Drawing.Size(277, 21);
            this.dtpConsumeDate.TabIndex = 0;
            this.dcm1.SetTagColumnName(this.dtpConsumeDate, null);
            this.dcm1.SetTextColumnName(this.dtpConsumeDate, "ConsumeDate");
            this.dtpConsumeDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtpConsumeDate_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(58, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "发生日期:";
            // 
            // comboxList1
            // 
            this.comboxList1.GridViewDataSource = null;
            this.comboxList1.MaxRowCount = 5;
            // 
            // FrmJiZhang
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(473, 310);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtpConsumeDate);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtConsumeType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtConsumeName);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmJiZhang";
            this.Text = "记账";
            this.Load += new System.EventHandler(this.FrmJiZhang_Load);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.txtConsumeName, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.txtAccount, 0);
            this.Controls.SetChildIndex(this.label3, 0);
            this.Controls.SetChildIndex(this.txtConsumeType, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.txtAmount, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.txtRemark, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnClose, 0);
            this.Controls.SetChildIndex(this.dtpConsumeDate, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private ZCTextBox txtConsumeName;
        private System.Windows.Forms.Label label2;
        private ZCTextBox txtAccount;
        private System.Windows.Forms.Label label3;
        private ZCTextBox txtConsumeType;
        private System.Windows.Forms.Label label4;
        private ZCTextBox txtAmount;
        private System.Windows.Forms.Label label5;
        private ZCTextBox txtRemark;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DateTimePicker dtpConsumeDate;
        private System.Windows.Forms.Label label6;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private ZhCun.Framework.WinCommon.Components.ComboxList comboxList1;
    }
}