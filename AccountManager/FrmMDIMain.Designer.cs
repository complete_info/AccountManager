﻿namespace AccountManager
{
    partial class FrmMDIMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            WeifenLuo.WinFormsUI.Docking.DockPanelSkin dockPanelSkin4 = new WeifenLuo.WinFormsUI.Docking.DockPanelSkin();
            WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin autoHideStripSkin4 = new WeifenLuo.WinFormsUI.Docking.AutoHideStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient10 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient22 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin dockPaneStripSkin4 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripSkin();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient dockPaneStripGradient4 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient23 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient11 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient24 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient dockPaneStripToolWindowGradient4 = new WeifenLuo.WinFormsUI.Docking.DockPaneStripToolWindowGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient25 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient26 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.DockPanelGradient dockPanelGradient12 = new WeifenLuo.WinFormsUI.Docking.DockPanelGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient27 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            WeifenLuo.WinFormsUI.Docking.TabGradient tabGradient28 = new WeifenLuo.WinFormsUI.Docking.TabGradient();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMDIMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.基本资料ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuOperator = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuConsumtType = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenutOutAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuJiZhang = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuDuiZhang = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuOutAccount = new System.Windows.Forms.ToolStripMenuItem();
            this.查询统计ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuZongHeYeWuMingXi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLeiXingTongJi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuLeiXingRiTongJi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuZhangHuRiTongJi = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuZiJinRiTongJi = new System.Windows.Forms.ToolStripMenuItem();
            this.系统管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuDataBak = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuChangeLoginInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuChangePwd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuChangeUser = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuAutoUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this.tsBtnHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuHelper = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuQQMe = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbUserName = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbTest = new System.Windows.Forms.ToolStripStatusLabel();
            this.dockPanel1 = new WeifenLuo.WinFormsUI.Docking.DockPanel();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tsBtnAccount = new System.Windows.Forms.ToolStripButton();
            this.tsBtnConsumeType = new System.Windows.Forms.ToolStripButton();
            this.tsBtnJiZhang = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDuiZhang = new System.Windows.Forms.ToolStripButton();
            this.tsBtnOutAccount = new System.Windows.Forms.ToolStripButton();
            this.tsBtnLock = new System.Windows.Forms.ToolStripButton();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.基本资料ToolStripMenuItem,
            this.tsMenutOutAccount,
            this.查询统计ToolStripMenuItem,
            this.系统管理ToolStripMenuItem,
            this.tsBtnHelp,
            this.tsMenuAbout});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1093, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 基本资料ToolStripMenuItem
            // 
            this.基本资料ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuOperator,
            this.tsMenuAccount,
            this.tsMenuConsumtType});
            this.基本资料ToolStripMenuItem.Name = "基本资料ToolStripMenuItem";
            this.基本资料ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.基本资料ToolStripMenuItem.Text = "基本资料";
            // 
            // tsMenuOperator
            // 
            this.tsMenuOperator.Name = "tsMenuOperator";
            this.tsMenuOperator.Size = new System.Drawing.Size(148, 22);
            this.tsMenuOperator.Text = "操作员管理";
            this.tsMenuOperator.Click += new System.EventHandler(this.tsMenuOperator_Click);
            // 
            // tsMenuAccount
            // 
            this.tsMenuAccount.Name = "tsMenuAccount";
            this.tsMenuAccount.Size = new System.Drawing.Size(148, 22);
            this.tsMenuAccount.Text = "账户管理";
            this.tsMenuAccount.Click += new System.EventHandler(this.tsMenuAccount_Click);
            // 
            // tsMenuConsumtType
            // 
            this.tsMenuConsumtType.Name = "tsMenuConsumtType";
            this.tsMenuConsumtType.Size = new System.Drawing.Size(148, 22);
            this.tsMenuConsumtType.Text = "账目类型管理";
            this.tsMenuConsumtType.Click += new System.EventHandler(this.tsMenuConsumtType_Click);
            // 
            // tsMenutOutAccount
            // 
            this.tsMenutOutAccount.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuJiZhang,
            this.tsMenuDuiZhang,
            this.tsMenuOutAccount});
            this.tsMenutOutAccount.Name = "tsMenutOutAccount";
            this.tsMenutOutAccount.Size = new System.Drawing.Size(68, 21);
            this.tsMenutOutAccount.Text = "日常操作";
            // 
            // tsMenuJiZhang
            // 
            this.tsMenuJiZhang.Name = "tsMenuJiZhang";
            this.tsMenuJiZhang.Size = new System.Drawing.Size(124, 22);
            this.tsMenuJiZhang.Text = "记账管理";
            this.tsMenuJiZhang.Click += new System.EventHandler(this.tsMenuJiZhang_Click);
            // 
            // tsMenuDuiZhang
            // 
            this.tsMenuDuiZhang.Name = "tsMenuDuiZhang";
            this.tsMenuDuiZhang.Size = new System.Drawing.Size(124, 22);
            this.tsMenuDuiZhang.Text = "账户对账";
            this.tsMenuDuiZhang.Click += new System.EventHandler(this.tsMenuDuiZhang_Click);
            // 
            // tsMenuOutAccount
            // 
            this.tsMenuOutAccount.Name = "tsMenuOutAccount";
            this.tsMenuOutAccount.Size = new System.Drawing.Size(124, 22);
            this.tsMenuOutAccount.Text = "账户转账";
            this.tsMenuOutAccount.Click += new System.EventHandler(this.tsMenuOutAccount_Click);
            // 
            // 查询统计ToolStripMenuItem
            // 
            this.查询统计ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuZongHeYeWuMingXi,
            this.tsLeiXingTongJi,
            this.tsMenuLeiXingRiTongJi,
            this.tsMenuZhangHuRiTongJi,
            this.tsMenuZiJinRiTongJi});
            this.查询统计ToolStripMenuItem.Name = "查询统计ToolStripMenuItem";
            this.查询统计ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.查询统计ToolStripMenuItem.Text = "查询统计";
            // 
            // tsMenuZongHeYeWuMingXi
            // 
            this.tsMenuZongHeYeWuMingXi.Name = "tsMenuZongHeYeWuMingXi";
            this.tsMenuZongHeYeWuMingXi.Size = new System.Drawing.Size(180, 22);
            this.tsMenuZongHeYeWuMingXi.Text = "综合业务明细";
            this.tsMenuZongHeYeWuMingXi.Click += new System.EventHandler(this.tsMenuZongHeYeWuMingXi_Click);
            // 
            // tsLeiXingTongJi
            // 
            this.tsLeiXingTongJi.Name = "tsLeiXingTongJi";
            this.tsLeiXingTongJi.Size = new System.Drawing.Size(180, 22);
            this.tsLeiXingTongJi.Text = "账目类型统计";
            this.tsLeiXingTongJi.Click += new System.EventHandler(this.tsLeiXingTongJi_Click);
            // 
            // tsMenuLeiXingRiTongJi
            // 
            this.tsMenuLeiXingRiTongJi.Name = "tsMenuLeiXingRiTongJi";
            this.tsMenuLeiXingRiTongJi.Size = new System.Drawing.Size(180, 22);
            this.tsMenuLeiXingRiTongJi.Text = "账目类型统计(日报)";
            this.tsMenuLeiXingRiTongJi.Click += new System.EventHandler(this.tsMenuLeiXingRiTongJi_Click);
            // 
            // tsMenuZhangHuRiTongJi
            // 
            this.tsMenuZhangHuRiTongJi.Name = "tsMenuZhangHuRiTongJi";
            this.tsMenuZhangHuRiTongJi.Size = new System.Drawing.Size(180, 22);
            this.tsMenuZhangHuRiTongJi.Text = "账户统计(日报)";
            this.tsMenuZhangHuRiTongJi.Click += new System.EventHandler(this.tsMenuZhangHuRiTongJi_Click);
            // 
            // tsMenuZiJinRiTongJi
            // 
            this.tsMenuZiJinRiTongJi.Name = "tsMenuZiJinRiTongJi";
            this.tsMenuZiJinRiTongJi.Size = new System.Drawing.Size(180, 22);
            this.tsMenuZiJinRiTongJi.Text = "资金统计(日报)";
            this.tsMenuZiJinRiTongJi.Click += new System.EventHandler(this.tsMenuZiJinRiTongJi_Click);
            // 
            // 系统管理ToolStripMenuItem
            // 
            this.系统管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuDataBak,
            this.tsMenuChangeLoginInfo,
            this.tsMenuChangePwd,
            this.tsMenuChangeUser,
            this.tsMenuAutoUpdate});
            this.系统管理ToolStripMenuItem.Name = "系统管理ToolStripMenuItem";
            this.系统管理ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.系统管理ToolStripMenuItem.Text = "系统管理";
            // 
            // tsMenuDataBak
            // 
            this.tsMenuDataBak.Name = "tsMenuDataBak";
            this.tsMenuDataBak.Size = new System.Drawing.Size(148, 22);
            this.tsMenuDataBak.Text = "数据维护";
            this.tsMenuDataBak.Click += new System.EventHandler(this.tsMenuDataBak_Click);
            // 
            // tsMenuChangeLoginInfo
            // 
            this.tsMenuChangeLoginInfo.Name = "tsMenuChangeLoginInfo";
            this.tsMenuChangeLoginInfo.Size = new System.Drawing.Size(148, 22);
            this.tsMenuChangeLoginInfo.Text = "修改个人信息";
            this.tsMenuChangeLoginInfo.Click += new System.EventHandler(this.tsMenuChangeLoginInfo_Click);
            // 
            // tsMenuChangePwd
            // 
            this.tsMenuChangePwd.Name = "tsMenuChangePwd";
            this.tsMenuChangePwd.Size = new System.Drawing.Size(148, 22);
            this.tsMenuChangePwd.Text = "修改密码";
            this.tsMenuChangePwd.Click += new System.EventHandler(this.tsMenuChangePwd_Click);
            // 
            // tsMenuChangeUser
            // 
            this.tsMenuChangeUser.Name = "tsMenuChangeUser";
            this.tsMenuChangeUser.Size = new System.Drawing.Size(148, 22);
            this.tsMenuChangeUser.Text = "切换用户";
            this.tsMenuChangeUser.Click += new System.EventHandler(this.tsMenuChangeUser_Click);
            // 
            // tsMenuAutoUpdate
            // 
            this.tsMenuAutoUpdate.Name = "tsMenuAutoUpdate";
            this.tsMenuAutoUpdate.Size = new System.Drawing.Size(148, 22);
            this.tsMenuAutoUpdate.Text = "程序更新";
            this.tsMenuAutoUpdate.Click += new System.EventHandler(this.tsMenuAutoUpdate_Click);
            // 
            // tsBtnHelp
            // 
            this.tsBtnHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMenuHelper,
            this.tsMenuQQMe});
            this.tsBtnHelp.Name = "tsBtnHelp";
            this.tsBtnHelp.Size = new System.Drawing.Size(44, 21);
            this.tsBtnHelp.Text = "帮助";
            this.tsBtnHelp.Click += new System.EventHandler(this.tsBtnHelp_Click);
            // 
            // tsMenuHelper
            // 
            this.tsMenuHelper.Name = "tsMenuHelper";
            this.tsMenuHelper.Size = new System.Drawing.Size(124, 22);
            this.tsMenuHelper.Text = "帮助页面";
            this.tsMenuHelper.Click += new System.EventHandler(this.tsMenuHelper_Click);
            // 
            // tsMenuQQMe
            // 
            this.tsMenuQQMe.Name = "tsMenuQQMe";
            this.tsMenuQQMe.Size = new System.Drawing.Size(124, 22);
            this.tsMenuQQMe.Text = "QQ作者";
            this.tsMenuQQMe.Click += new System.EventHandler(this.tsMenuQQMe_Click);
            // 
            // tsMenuAbout
            // 
            this.tsMenuAbout.Name = "tsMenuAbout";
            this.tsMenuAbout.Size = new System.Drawing.Size(44, 21);
            this.tsMenuAbout.Text = "关于";
            this.tsMenuAbout.Click += new System.EventHandler(this.tsMenuAbout_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnAccount,
            this.tsBtnConsumeType,
            this.toolStripSeparator1,
            this.tsBtnJiZhang,
            this.tsBtnDuiZhang,
            this.tsBtnOutAccount,
            this.tsBtnLock});
            this.toolStrip1.Location = new System.Drawing.Point(0, 25);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1093, 38);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 38);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslbUserName,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabel4,
            this.tslbLoginTime,
            this.toolStripStatusLabel2,
            this.lbTest});
            this.statusStrip1.Location = new System.Drawing.Point(0, 660);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1093, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(35, 17);
            this.toolStripStatusLabel1.Text = "用户:";
            // 
            // tslbUserName
            // 
            this.tslbUserName.BackColor = System.Drawing.Color.Transparent;
            this.tslbUserName.ForeColor = System.Drawing.Color.Red;
            this.tslbUserName.Name = "tslbUserName";
            this.tslbUserName.Size = new System.Drawing.Size(44, 17);
            this.tslbUserName.Text = "管理员";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel3.Text = "|";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel4.Text = "登录时间:";
            // 
            // tslbLoginTime
            // 
            this.tslbLoginTime.BackColor = System.Drawing.Color.Transparent;
            this.tslbLoginTime.ForeColor = System.Drawing.Color.Red;
            this.tslbLoginTime.Name = "tslbLoginTime";
            this.tslbLoginTime.Size = new System.Drawing.Size(119, 17);
            this.tslbLoginTime.Text = "2015-1-16 15:52:55";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // lbTest
            // 
            this.lbTest.BackColor = System.Drawing.Color.Transparent;
            this.lbTest.ForeColor = System.Drawing.Color.Red;
            this.lbTest.Name = "lbTest";
            this.lbTest.Size = new System.Drawing.Size(0, 17);
            // 
            // dockPanel1
            // 
            this.dockPanel1.ActiveAutoHideContent = null;
            this.dockPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dockPanel1.DockBackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dockPanel1.Location = new System.Drawing.Point(0, 63);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Size = new System.Drawing.Size(1093, 597);
            dockPanelGradient10.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient10.StartColor = System.Drawing.SystemColors.ControlLight;
            autoHideStripSkin4.DockStripGradient = dockPanelGradient10;
            tabGradient22.EndColor = System.Drawing.SystemColors.Control;
            tabGradient22.StartColor = System.Drawing.SystemColors.Control;
            tabGradient22.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            autoHideStripSkin4.TabGradient = tabGradient22;
            autoHideStripSkin4.TextFont = new System.Drawing.Font("微软雅黑", 9F);
            dockPanelSkin4.AutoHideStripSkin = autoHideStripSkin4;
            tabGradient23.EndColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient23.StartColor = System.Drawing.SystemColors.ControlLightLight;
            tabGradient23.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient4.ActiveTabGradient = tabGradient23;
            dockPanelGradient11.EndColor = System.Drawing.SystemColors.Control;
            dockPanelGradient11.StartColor = System.Drawing.SystemColors.Control;
            dockPaneStripGradient4.DockStripGradient = dockPanelGradient11;
            tabGradient24.EndColor = System.Drawing.SystemColors.ControlLight;
            tabGradient24.StartColor = System.Drawing.SystemColors.ControlLight;
            tabGradient24.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripGradient4.InactiveTabGradient = tabGradient24;
            dockPaneStripSkin4.DocumentGradient = dockPaneStripGradient4;
            dockPaneStripSkin4.TextFont = new System.Drawing.Font("微软雅黑", 9F);
            tabGradient25.EndColor = System.Drawing.SystemColors.ActiveCaption;
            tabGradient25.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient25.StartColor = System.Drawing.SystemColors.GradientActiveCaption;
            tabGradient25.TextColor = System.Drawing.SystemColors.ActiveCaptionText;
            dockPaneStripToolWindowGradient4.ActiveCaptionGradient = tabGradient25;
            tabGradient26.EndColor = System.Drawing.SystemColors.Control;
            tabGradient26.StartColor = System.Drawing.SystemColors.Control;
            tabGradient26.TextColor = System.Drawing.SystemColors.ControlText;
            dockPaneStripToolWindowGradient4.ActiveTabGradient = tabGradient26;
            dockPanelGradient12.EndColor = System.Drawing.SystemColors.ControlLight;
            dockPanelGradient12.StartColor = System.Drawing.SystemColors.ControlLight;
            dockPaneStripToolWindowGradient4.DockStripGradient = dockPanelGradient12;
            tabGradient27.EndColor = System.Drawing.SystemColors.InactiveCaption;
            tabGradient27.LinearGradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            tabGradient27.StartColor = System.Drawing.SystemColors.GradientInactiveCaption;
            tabGradient27.TextColor = System.Drawing.SystemColors.InactiveCaptionText;
            dockPaneStripToolWindowGradient4.InactiveCaptionGradient = tabGradient27;
            tabGradient28.EndColor = System.Drawing.Color.Transparent;
            tabGradient28.StartColor = System.Drawing.Color.Transparent;
            tabGradient28.TextColor = System.Drawing.SystemColors.ControlDarkDark;
            dockPaneStripToolWindowGradient4.InactiveTabGradient = tabGradient28;
            dockPaneStripSkin4.ToolWindowGradient = dockPaneStripToolWindowGradient4;
            dockPanelSkin4.DockPaneStripSkin = dockPaneStripSkin4;
            this.dockPanel1.Skin = dockPanelSkin4;
            this.dockPanel1.TabIndex = 9;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "个人记账软件";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // tsBtnAccount
            // 
            this.tsBtnAccount.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAccount.Image = global::AccountManager.Properties.Resources.account;
            this.tsBtnAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAccount.Name = "tsBtnAccount";
            this.tsBtnAccount.Size = new System.Drawing.Size(101, 35);
            this.tsBtnAccount.Text = "账户管理";
            this.tsBtnAccount.Click += new System.EventHandler(this.tsBtnAccount_Click);
            // 
            // tsBtnConsumeType
            // 
            this.tsBtnConsumeType.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnConsumeType.Image = global::AccountManager.Properties.Resources.chongzhang;
            this.tsBtnConsumeType.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnConsumeType.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnConsumeType.Name = "tsBtnConsumeType";
            this.tsBtnConsumeType.Size = new System.Drawing.Size(101, 35);
            this.tsBtnConsumeType.Text = "账目类型";
            this.tsBtnConsumeType.Click += new System.EventHandler(this.tsBtnConsumeType_Click);
            // 
            // tsBtnJiZhang
            // 
            this.tsBtnJiZhang.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnJiZhang.Image = global::AccountManager.Properties.Resources.jizhang;
            this.tsBtnJiZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnJiZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnJiZhang.Name = "tsBtnJiZhang";
            this.tsBtnJiZhang.Size = new System.Drawing.Size(73, 35);
            this.tsBtnJiZhang.Text = "记账";
            this.tsBtnJiZhang.Click += new System.EventHandler(this.tsBtnJiZhang_Click);
            // 
            // tsBtnDuiZhang
            // 
            this.tsBtnDuiZhang.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnDuiZhang.Image = global::AccountManager.Properties.Resources.book;
            this.tsBtnDuiZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnDuiZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDuiZhang.Name = "tsBtnDuiZhang";
            this.tsBtnDuiZhang.Size = new System.Drawing.Size(73, 35);
            this.tsBtnDuiZhang.Text = "对账";
            this.tsBtnDuiZhang.Click += new System.EventHandler(this.tsBtnDuiZhang_Click);
            // 
            // tsBtnOutAccount
            // 
            this.tsBtnOutAccount.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnOutAccount.Image = global::AccountManager.Properties.Resources.fuzhijizhang;
            this.tsBtnOutAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnOutAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnOutAccount.Name = "tsBtnOutAccount";
            this.tsBtnOutAccount.Size = new System.Drawing.Size(73, 35);
            this.tsBtnOutAccount.Text = "转账";
            this.tsBtnOutAccount.Click += new System.EventHandler(this.tsBtnOutAccount_Click);
            // 
            // tsBtnLock
            // 
            this.tsBtnLock.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnLock.Image = global::AccountManager.Properties.Resources._lock;
            this.tsBtnLock.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnLock.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnLock.Name = "tsBtnLock";
            this.tsBtnLock.Size = new System.Drawing.Size(73, 35);
            this.tsBtnLock.Text = "锁定";
            this.tsBtnLock.Click += new System.EventHandler(this.tsBtnLock_Click);
            // 
            // FrmMDIMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 682);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMDIMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "个人记账软件";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMDIMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMDIMain_Load);
            this.SizeChanged += new System.EventHandler(this.FrmMDIMain_SizeChanged);
            this.Controls.SetChildIndex(this.menuStrip1, 0);
            this.Controls.SetChildIndex(this.toolStrip1, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.dockPanel1, 0);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 基本资料ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsMenuOperator;
        private System.Windows.Forms.ToolStripMenuItem tsMenuAccount;
        private System.Windows.Forms.ToolStripMenuItem tsMenuConsumtType;
        private System.Windows.Forms.ToolStripMenuItem tsMenutOutAccount;
        private System.Windows.Forms.ToolStripMenuItem tsMenuJiZhang;
        private System.Windows.Forms.ToolStripMenuItem tsMenuDuiZhang;
        private System.Windows.Forms.ToolStripMenuItem 查询统计ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 系统管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsMenuDataBak;
        private System.Windows.Forms.ToolStripMenuItem tsMenuZongHeYeWuMingXi;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private WeifenLuo.WinFormsUI.Docking.DockPanel dockPanel1;
        private System.Windows.Forms.ToolStripButton tsBtnAccount;
        private System.Windows.Forms.ToolStripButton tsBtnConsumeType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsBtnJiZhang;
        private System.Windows.Forms.ToolStripButton tsBtnDuiZhang;
        private System.Windows.Forms.ToolStripMenuItem tsLeiXingTongJi;
        private System.Windows.Forms.ToolStripMenuItem tsMenuLeiXingRiTongJi;
        private System.Windows.Forms.ToolStripMenuItem tsMenuZhangHuRiTongJi;
        private System.Windows.Forms.ToolStripButton tsBtnOutAccount;
        private System.Windows.Forms.ToolStripMenuItem tsMenuOutAccount;
        private System.Windows.Forms.ToolStripMenuItem tsMenuZiJinRiTongJi;
        private System.Windows.Forms.ToolStripMenuItem tsMenuChangeLoginInfo;
        private System.Windows.Forms.ToolStripMenuItem tsMenuChangePwd;
        private System.Windows.Forms.ToolStripMenuItem tsMenuChangeUser;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslbUserName;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel tslbLoginTime;
        private System.Windows.Forms.ToolStripMenuItem tsMenuAbout;
        private System.Windows.Forms.ToolStripMenuItem tsMenuAutoUpdate;
        private System.Windows.Forms.ToolStripMenuItem tsBtnHelp;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem tsMenuHelper;
        private System.Windows.Forms.ToolStripMenuItem tsMenuQQMe;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripStatusLabel lbTest;
        private System.Windows.Forms.ToolStripButton tsBtnLock;
    }
}