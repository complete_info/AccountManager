﻿namespace AccountManager
{
    partial class FrmJiZhangView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnOutBalance = new System.Windows.Forms.ToolStripButton();
            this.tsBtnInBalance = new System.Windows.Forms.ToolStripButton();
            this.tsBtnClose = new System.Windows.Forms.ToolStripButton();
            this.tsBtnCopyJiZhang = new System.Windows.Forms.ToolStripButton();
            this.tsBtnUndoLastAccount = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnChongZhang = new System.Windows.Forms.ToolStripButton();
            this.btnConsumeEdit = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdoConsumeTime = new System.Windows.Forms.RadioButton();
            this.rdoAddTime = new System.Windows.Forms.RadioButton();
            this.txtAccount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.coBoxBalanceType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtConsumeName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.comboxList1 = new ZhCun.Framework.WinCommon.Components.ComboxList(this.components);
            this.col_ConsumeDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_BalanceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AddTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_IsCancel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripTop1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripTop1
            // 
            this.toolStripTop1.AutoSize = false;
            this.toolStripTop1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnOutBalance,
            this.tsBtnInBalance,
            this.tsBtnClose,
            this.tsBtnCopyJiZhang,
            this.tsBtnUndoLastAccount,
            this.toolStripSeparator5,
            this.btnChongZhang,
            this.btnConsumeEdit});
            this.toolStripTop1.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop1.Name = "toolStripTop1";
            this.toolStripTop1.Size = new System.Drawing.Size(996, 38);
            this.toolStripTop1.TabIndex = 19;
            this.toolStripTop1.Text = "toolStrip2";
            // 
            // tsBtnOutBalance
            // 
            this.tsBtnOutBalance.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnOutBalance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnOutBalance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnOutBalance.Image = global::AccountManager.Properties.Resources.zhichu;
            this.tsBtnOutBalance.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnOutBalance.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnOutBalance.Name = "tsBtnOutBalance";
            this.tsBtnOutBalance.Size = new System.Drawing.Size(92, 35);
            this.tsBtnOutBalance.Text = "支出记账";
            this.tsBtnOutBalance.Click += new System.EventHandler(this.tsBtnOutBalance_Click);
            // 
            // tsBtnInBalance
            // 
            this.tsBtnInBalance.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnInBalance.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnInBalance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnInBalance.Image = global::AccountManager.Properties.Resources.shouru;
            this.tsBtnInBalance.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnInBalance.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnInBalance.Name = "tsBtnInBalance";
            this.tsBtnInBalance.Size = new System.Drawing.Size(92, 35);
            this.tsBtnInBalance.Text = "收入记账";
            this.tsBtnInBalance.Click += new System.EventHandler(this.tsBtnInBalance_Click);
            // 
            // tsBtnClose
            // 
            this.tsBtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnClose.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnClose.Image = global::AccountManager.Properties.Resources.exit;
            this.tsBtnClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnClose.Margin = new System.Windows.Forms.Padding(0, 1, 1, 2);
            this.tsBtnClose.Name = "tsBtnClose";
            this.tsBtnClose.Padding = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.tsBtnClose.Size = new System.Drawing.Size(74, 35);
            this.tsBtnClose.Text = "退出";
            this.tsBtnClose.Click += new System.EventHandler(this.tsBtnClose_Click);
            // 
            // tsBtnCopyJiZhang
            // 
            this.tsBtnCopyJiZhang.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnCopyJiZhang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnCopyJiZhang.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnCopyJiZhang.Image = global::AccountManager.Properties.Resources.chongzhang;
            this.tsBtnCopyJiZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnCopyJiZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnCopyJiZhang.Name = "tsBtnCopyJiZhang";
            this.tsBtnCopyJiZhang.Size = new System.Drawing.Size(92, 35);
            this.tsBtnCopyJiZhang.Text = "流水记账";
            this.tsBtnCopyJiZhang.Click += new System.EventHandler(this.tsBtnCopyJiZhang_Click);
            // 
            // tsBtnUndoLastAccount
            // 
            this.tsBtnUndoLastAccount.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnUndoLastAccount.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnUndoLastAccount.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnUndoLastAccount.Image = global::AccountManager.Properties.Resources.Undo;
            this.tsBtnUndoLastAccount.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnUndoLastAccount.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnUndoLastAccount.Name = "tsBtnUndoLastAccount";
            this.tsBtnUndoLastAccount.Size = new System.Drawing.Size(92, 35);
            this.tsBtnUndoLastAccount.Text = "撤销记账";
            this.tsBtnUndoLastAccount.ToolTipText = "撤销当天最后一笔记账记录";
            this.tsBtnUndoLastAccount.Click += new System.EventHandler(this.tsBtnUndoLastAccount_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 38);
            // 
            // btnChongZhang
            // 
            this.btnChongZhang.BackColor = System.Drawing.Color.Transparent;
            this.btnChongZhang.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnChongZhang.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnChongZhang.Image = global::AccountManager.Properties.Resources.fuzhijizhang;
            this.btnChongZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnChongZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnChongZhang.Name = "btnChongZhang";
            this.btnChongZhang.Size = new System.Drawing.Size(92, 35);
            this.btnChongZhang.Text = "流水冲账";
            this.btnChongZhang.Click += new System.EventHandler(this.btnChongZhang_Click);
            // 
            // btnConsumeEdit
            // 
            this.btnConsumeEdit.BackColor = System.Drawing.Color.Transparent;
            this.btnConsumeEdit.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnConsumeEdit.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnConsumeEdit.Image = global::AccountManager.Properties.Resources.edit;
            this.btnConsumeEdit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.btnConsumeEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConsumeEdit.Name = "btnConsumeEdit";
            this.btnConsumeEdit.Size = new System.Drawing.Size(92, 35);
            this.btnConsumeEdit.Text = "流水编辑";
            this.btnConsumeEdit.Click += new System.EventHandler(this.btnConsumeEdit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtAccount);
            this.groupBox1.Controls.Add(this.txtConsumeType);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.coBoxBalanceType);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtConsumeName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(996, 111);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "检索";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdoConsumeTime);
            this.groupBox2.Controls.Add(this.rdoAddTime);
            this.groupBox2.Location = new System.Drawing.Point(245, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(112, 71);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "日期选项";
            // 
            // rdoConsumeTime
            // 
            this.rdoConsumeTime.AutoSize = true;
            this.rdoConsumeTime.Location = new System.Drawing.Point(16, 47);
            this.rdoConsumeTime.Name = "rdoConsumeTime";
            this.rdoConsumeTime.Size = new System.Drawing.Size(71, 16);
            this.rdoConsumeTime.TabIndex = 8;
            this.rdoConsumeTime.Text = "发生时间";
            this.rdoConsumeTime.UseVisualStyleBackColor = true;
            // 
            // rdoAddTime
            // 
            this.rdoAddTime.AutoSize = true;
            this.rdoAddTime.Checked = true;
            this.rdoAddTime.Location = new System.Drawing.Point(16, 20);
            this.rdoAddTime.Name = "rdoAddTime";
            this.rdoAddTime.Size = new System.Drawing.Size(71, 16);
            this.rdoAddTime.TabIndex = 8;
            this.rdoAddTime.TabStop = true;
            this.rdoAddTime.Text = "记账时间";
            this.rdoAddTime.UseVisualStyleBackColor = true;
            // 
            // txtAccount
            // 
            this.comboxList1.SetColumns(this.txtAccount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccount, 0);
            this.txtAccount.EmptyTextTip = "回车选择账户";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccount, 0);
            this.dcm1.SetIsUse(this.txtAccount, true);
            this.comboxList1.SetIsUse(this.txtAccount, false);
            this.txtAccount.Location = new System.Drawing.Point(693, 68);
            this.txtAccount.Name = "txtAccount";
            this.comboxList1.SetNextControl(this.txtAccount, this.txtAccount);
            this.txtAccount.Size = new System.Drawing.Size(152, 21);
            this.txtAccount.TabIndex = 11;
            this.dcm1.SetTagColumnName(this.txtAccount, "Account");
            this.dcm1.SetTextColumnName(this.txtAccount, "");
            // 
            // txtConsumeType
            // 
            this.comboxList1.SetColumns(this.txtConsumeType, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeType, 0);
            this.txtConsumeType.EmptyTextTip = "回车选择消费类型";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeType, 0);
            this.dcm1.SetIsUse(this.txtConsumeType, true);
            this.comboxList1.SetIsUse(this.txtConsumeType, false);
            this.txtConsumeType.Location = new System.Drawing.Point(448, 67);
            this.txtConsumeType.Name = "txtConsumeType";
            this.comboxList1.SetNextControl(this.txtConsumeType, this.txtConsumeType);
            this.txtConsumeType.Size = new System.Drawing.Size(152, 21);
            this.txtConsumeType.TabIndex = 11;
            this.dcm1.SetTagColumnName(this.txtConsumeType, "ConsumeType");
            this.dcm1.SetTextColumnName(this.txtConsumeType, "");
            this.txtConsumeType.TextChanged += new System.EventHandler(this.txtConsumeType_TextChanged);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(864, 68);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 25);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "清空";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(864, 28);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 25);
            this.btnSearch.TabIndex = 10;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // coBoxBalanceType
            // 
            this.coBoxBalanceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.coBoxBalanceType.FormattingEnabled = true;
            this.dcm1.SetIsUse(this.coBoxBalanceType, true);
            this.coBoxBalanceType.Location = new System.Drawing.Point(693, 34);
            this.coBoxBalanceType.Name = "coBoxBalanceType";
            this.coBoxBalanceType.Size = new System.Drawing.Size(152, 20);
            this.coBoxBalanceType.TabIndex = 9;
            this.dcm1.SetTagColumnName(this.coBoxBalanceType, "BalanceType");
            this.dcm1.SetTextColumnName(this.coBoxBalanceType, null);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(652, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "账户:";
            // 
            // txtConsumeName
            // 
            this.comboxList1.SetColumns(this.txtConsumeName, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeName, 0);
            this.comboxList1.SetGridViewWidth(this.txtConsumeName, 0);
            this.comboxList1.SetIsUse(this.txtConsumeName, false);
            this.dcm1.SetIsUse(this.txtConsumeName, true);
            this.txtConsumeName.Location = new System.Drawing.Point(448, 34);
            this.txtConsumeName.Name = "txtConsumeName";
            this.comboxList1.SetNextControl(this.txtConsumeName, this.txtConsumeName);
            this.txtConsumeName.Size = new System.Drawing.Size(152, 21);
            this.txtConsumeName.TabIndex = 3;
            this.dcm1.SetTagColumnName(this.txtConsumeName, null);
            this.dcm1.SetTextColumnName(this.txtConsumeName, "ConsumeName");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(382, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "消费类型:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(628, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "资金类型:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(383, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "消费说明:";
            // 
            // dtpEndDate
            // 
            this.dcm1.SetIsUse(this.dtpEndDate, true);
            this.dtpEndDate.Location = new System.Drawing.Point(85, 66);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(133, 21);
            this.dtpEndDate.TabIndex = 1;
            this.dcm1.SetTagColumnName(this.dtpEndDate, null);
            this.dcm1.SetTextColumnName(this.dtpEndDate, "EndTime");
            // 
            // dtpStartDate
            // 
            this.dcm1.SetIsUse(this.dtpStartDate, true);
            this.dtpStartDate.Location = new System.Drawing.Point(85, 31);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(133, 21);
            this.dtpStartDate.TabIndex = 1;
            this.dcm1.SetTagColumnName(this.dtpStartDate, null);
            this.dcm1.SetTextColumnName(this.dtpStartDate, "StartTime");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "结束日期:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "开始日期:";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ConsumeDate,
            this.col_ConsumeName,
            this.col_AccountName,
            this.col_BalanceType,
            this.col_ConsumeType,
            this.col_Amount,
            this.col_Balance,
            this.col_AddTime,
            this.col_IsCancel,
            this.col_Remark});
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 149);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(996, 514);
            this.dgv.TabIndex = 23;
            this.dgv.UseControlStyle = true;
            // 
            // comboxList1
            // 
            this.comboxList1.GridViewDataSource = null;
            this.comboxList1.MaxRowCount = 5;
            // 
            // col_ConsumeDate
            // 
            this.col_ConsumeDate.DataPropertyName = "ConsumeDate";
            dataGridViewCellStyle2.Format = "yyyy-MM-dd";
            dataGridViewCellStyle2.NullValue = null;
            this.col_ConsumeDate.DefaultCellStyle = dataGridViewCellStyle2;
            this.col_ConsumeDate.HeaderText = "账目日期";
            this.col_ConsumeDate.Name = "col_ConsumeDate";
            this.col_ConsumeDate.ReadOnly = true;
            // 
            // col_ConsumeName
            // 
            this.col_ConsumeName.DataPropertyName = "ConsumeName";
            this.col_ConsumeName.HeaderText = "账目说明";
            this.col_ConsumeName.Name = "col_ConsumeName";
            this.col_ConsumeName.ReadOnly = true;
            // 
            // col_AccountName
            // 
            this.col_AccountName.DataPropertyName = "AccountName";
            this.col_AccountName.HeaderText = "账户";
            this.col_AccountName.Name = "col_AccountName";
            this.col_AccountName.ReadOnly = true;
            // 
            // col_BalanceType
            // 
            this.col_BalanceType.DataPropertyName = "BalanceTypeName";
            this.col_BalanceType.HeaderText = "资金类型";
            this.col_BalanceType.Name = "col_BalanceType";
            this.col_BalanceType.ReadOnly = true;
            // 
            // col_ConsumeType
            // 
            this.col_ConsumeType.DataPropertyName = "ConsumeTypeName";
            this.col_ConsumeType.HeaderText = "账目类型";
            this.col_ConsumeType.Name = "col_ConsumeType";
            this.col_ConsumeType.ReadOnly = true;
            // 
            // col_Amount
            // 
            this.col_Amount.DataPropertyName = "Amount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "0";
            this.col_Amount.DefaultCellStyle = dataGridViewCellStyle3;
            this.col_Amount.HeaderText = "发生额";
            this.col_Amount.Name = "col_Amount";
            this.col_Amount.ReadOnly = true;
            // 
            // col_Balance
            // 
            this.col_Balance.DataPropertyName = "Balance";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "0";
            this.col_Balance.DefaultCellStyle = dataGridViewCellStyle4;
            this.col_Balance.HeaderText = "账户余额";
            this.col_Balance.Name = "col_Balance";
            this.col_Balance.ReadOnly = true;
            // 
            // col_AddTime
            // 
            this.col_AddTime.DataPropertyName = "AddTime";
            this.col_AddTime.HeaderText = "记账日期";
            this.col_AddTime.Name = "col_AddTime";
            this.col_AddTime.ReadOnly = true;
            // 
            // col_IsCancel
            // 
            this.col_IsCancel.DataPropertyName = "IsCancel";
            this.col_IsCancel.HeaderText = "冲账标志";
            this.col_IsCancel.Name = "col_IsCancel";
            this.col_IsCancel.ReadOnly = true;
            this.col_IsCancel.Visible = false;
            // 
            // col_Remark
            // 
            this.col_Remark.DataPropertyName = "Remark";
            this.col_Remark.HeaderText = "备注";
            this.col_Remark.Name = "col_Remark";
            this.col_Remark.ReadOnly = true;
            // 
            // FrmJiZhangView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(996, 663);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStripTop1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmJiZhangView";
            this.Text = "记账查看";
            this.Load += new System.EventHandler(this.FrmJiZhangView_Load);
            this.Controls.SetChildIndex(this.toolStripTop1, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.toolStripTop1.ResumeLayout(false);
            this.toolStripTop1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop1;
        public System.Windows.Forms.ToolStripButton tsBtnOutBalance;
        public System.Windows.Forms.ToolStripButton tsBtnInBalance;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.ToolStripButton tsBtnClose;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConsumeName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox coBoxBalanceType;
        private System.Windows.Forms.Button btnSearch;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtConsumeType;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccount;
        private System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.ToolStripButton btnChongZhang;
        public System.Windows.Forms.ToolStripButton btnConsumeEdit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdoConsumeTime;
        private System.Windows.Forms.RadioButton rdoAddTime;
        private ZhCun.Framework.WinCommon.Components.ComboxList comboxList1;
        public System.Windows.Forms.ToolStripButton tsBtnCopyJiZhang;
        public System.Windows.Forms.ToolStripButton tsBtnUndoLastAccount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AccountName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_BalanceType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeType;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AddTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_IsCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Remark;
    }
}