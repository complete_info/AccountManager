﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.TableModel.Model;
using ZhCun.Framework.WinAdvancedSearch;
using ZhCun.Framework.Common.Models;

namespace AccountManager.ChaXunTongJi
{
    public partial class FrmZongHeYeWuMingXi : BaseForm, IAdvancedSearch
    {
        public FrmZongHeYeWuMingXi()
        {
            InitializeComponent();
        }

        ALZongHeYeWuMingXi _ALObj;
        ALZongHeYeWuMingXi.ConsumeSearchModel _SearchModel;
        ISearch _ADSearchObj;

        private void FrmZongHeYeWuMingXi_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZongHeYeWuMingXi();
            _SearchModel = new ALZongHeYeWuMingXi.ConsumeSearchModel();
            coBoxIsCancel.SelectedIndex = 0;
            ComboxHelper.SetBalanceType(coBoxBalanceType);
            _ADSearchObj = _ALObj.CreateSearch();
        }
        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            dcm.SetValueToClassObj(_SearchModel);
            _SearchModel.ChongZhangBiaoZhi = coBoxIsCancel.SelectedIndex;
            List<VConsume> list = _ALObj.GetConsumeData(_SearchModel);
            if (list != null)
            {

                decimal sumAmount = 0;
                foreach (var item in list)
                {
                    sumAmount += item.Amount;
                }
                tslbSumAmount.Text = sumAmount.ToString();
                tslbRecordCount.Text = list.Count.ToString();
            }
            else
            {
                tslbSumAmount.Text = "0";
                tslbRecordCount.Text = "0";
            }
            dgv.DataSource = list;
        }

        private void btnAdwSearch_Click(object sender, EventArgs e)
        {
            FrmADSearch frm = new FrmADSearch(this, _ADSearchObj);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _ADSearchObj.OrderByDesc(VConsume.CNAddTime);
                List<VConsume> rList = _ALObj.GetConsumeData(_ADSearchObj);
                dgv.DataSource = rList;
            }
        }
    }
}
