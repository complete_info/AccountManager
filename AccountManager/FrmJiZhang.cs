﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using AccountManager.AL;
using AccountManager.JiChuZiLiao;
using AccountManager.AL.Enums;

namespace AccountManager
{
    public partial class FrmJiZhang : BaseForm
    {
        public FrmJiZhang(EditFormHandle<TConsume> AddConsumeHandle, EnBalanceType balanceType)
            : this(AddConsumeHandle, balanceType, null)
        { }
        public FrmJiZhang(EditFormHandle<TConsume> AddConsumeHandle, EnBalanceType balanceType, VConsume selModel)
            : this(AddConsumeHandle, balanceType, selModel, false)
        { }
        public FrmJiZhang(EditFormHandle<TConsume> AddConsumeHandle, EnBalanceType balanceType, VConsume selModel, bool isCopyJiZhang)
        {
            InitializeComponent();
            this.AddConsumeHandle = AddConsumeHandle;
            this._Model = selModel;
            this._BalanceType = balanceType;
            this._IsCopyJiZhang = isCopyJiZhang;
        }

        EnBalanceType _BalanceType;
        EditFormHandle<TConsume> AddConsumeHandle;
        VConsume _Model;
        bool _IsCopyJiZhang;

        private void FrmJiZhang_Load(object sender, EventArgs e)
        {
            if (_Model != null)
            {
                dcm1.SetControlValue(_Model);
                txtAccount.Enabled = false;
                txtAmount.Enabled = false;
            }
            switch (_BalanceType)
            {
                case EnBalanceType.ZhiChu:
                    ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, EnConsumeTypeCategory.ZhiChu);
                    break;
                case EnBalanceType.ShouRu:
                    ComboxListHelper.SetConsumeType(comboxList1, txtConsumeType, EnConsumeTypeCategory.ShouRu);
                    break;
                case EnBalanceType.ZhiChuChongZhang:
                case EnBalanceType.ShouRuChongZhang:
                case EnBalanceType.DuizhangZhiChu:
                case EnBalanceType.DuiZhangShouRu:
                default:
                    throw new Exception("记账类型参数有误!");
            }
            ComboxListHelper.SetAccount(comboxList1, txtAccount);
            //复制记账
            if (this._IsCopyJiZhang)
            {
                dtpConsumeDate.Value = DateTime.Now;
                txtAccount.Enabled = true;
                txtAmount.Enabled = true;
                txtAmount.Focus();
                txtAmount.SelectAll();
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            TConsume model = new TConsume();
            dcm1.SetValueToClassObj(model);
            string errMsg;
            bool r = AddConsumeHandle(model, out errMsg);
            if (!r)
            {
                ShowMessage(errMsg);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtConsumeName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtAccount.Focus();
            }
        }

        private void txtAmount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRemark.Focus();
            }
        }

        private void dtpConsumeDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtConsumeName.Focus();
            }
        }
    }
}