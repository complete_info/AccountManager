﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.AL;
using ZhCun.Framework.WinCommon.Components;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using AccountManager.AL.Enums;

namespace AccountManager
{
    public class ComboxListHelper
    {
        static ALComboxListHelper ALObj = new ALComboxListHelper();

        #region 账号信息
        
        public static void SetAccount(ComboxList cboxList, TextBox tb)
        {
            cboxList.SetColumns(tb, new ComBoxListColumn[] 
                {   
                    new ComBoxListColumn(){ FieldCaption="账户名称", FieldName=TAccount.CNActName ,IsReturnText = true},
                    new ComBoxListColumn(){ FieldCaption="余额", FieldName=TAccount.CNBalance },                    
                    new ComBoxListColumn(){ FieldCaption="Id", FieldName=TAccount.CNId,IsReturnTag = true,IsVisible=false}
                });
            cboxList.SetIsUse(tb, true);
            tb.KeyDown += new KeyEventHandler(delegate(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cboxList.GridViewDataSource = ALObj.GetAccountData(tb.Text);
                }
            });
        }

        #endregion

        #region 消费类型初始化

        /// <summary>
        /// 设置消费类型
        /// isShouRu=true,按收入否则支出
        /// balanceType 0:全部,1:支出,2:收入,3:对账
        /// </summary>
        public static void SetConsumeType(ComboxList cboxList, TextBox tb,EnConsumeTypeCategory typeCategory)
        {
            cboxList.SetColumns(tb, new ComBoxListColumn[] 
                {   
                    new ComBoxListColumn(){ FieldCaption="类型名称", FieldName=VConsumeType.CNTypeName ,IsReturnText = true},
                    new ComBoxListColumn(){ FieldCaption="助记码", FieldName=VConsumeType.CNTypeNamePY  },
                    new ComBoxListColumn(){ FieldCaption="资金类型", FieldName=VConsumeType.CNCategoryName  },
                    new ComBoxListColumn(){ FieldCaption="Id", FieldName=VConsumeType.CNId,IsReturnTag = true,IsVisible=false}
                });
            cboxList.SetIsUse(tb, true);
            tb.KeyDown += new KeyEventHandler(delegate(object sender, KeyEventArgs e)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cboxList.GridViewDataSource = ALObj.GetConsumeType(tb.Text, typeCategory);
                }
            });
        }

        #endregion
    }
}