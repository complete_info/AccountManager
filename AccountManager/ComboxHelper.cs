﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using System.Data;

namespace AccountManager
{
    public class ComboxHelper
    {
        static ComboxHelper()
        {
            _ALObj = new ALComboxHelper();
        }
        static ALComboxHelper _ALObj;

        /// <summary>
        /// 设置资金类型的下拉列表数据
        /// </summary>
        public static void SetBalanceType(ComboBox cobox)
        {
            DataTable dt = _ALObj.GetBalanceTypeData();
            cobox.DataSource = dt;
            cobox.ValueMember = dt.Columns[0].ColumnName;
            cobox.DisplayMember = dt.Columns[1].ColumnName;
            cobox.SelectedIndex = 0;
        }
        /// <summary>
        /// 账目类型分类
        /// </summary>
        public static void SetConsumeTypeCategory(ComboBox cobox)
        {
            DataTable dt = _ALObj.GetConsumeTypeCategory();
            //DataRow dr = dt.NewRow();
            //dr.ItemArray = new object[] { 0, "" };
            //dt.Rows.InsertAt(dr, 0);
            cobox.ValueMember = dt.Columns[0].ColumnName;
            cobox.DisplayMember = dt.Columns[1].ColumnName;
            cobox.DataSource = dt;
            cobox.SelectedIndex = 0;
        }
    }
}
